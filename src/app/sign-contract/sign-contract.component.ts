import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef} from '@angular/material';
import { OnboardingQuesComponent } from '../onboarding-ques/onboarding-ques.component'; 
import { ContractAgreementComponent } from '../contract-agreement/contract-agreement.component';
@Component({
  selector: 'app-sign-contract',
  templateUrl: './sign-contract.component.html',
  styleUrls: ['./sign-contract.component.css']
})
export class SignContractComponent implements OnInit {
  // fileNameDialogRef: MatDialogRef<OnboardingQuesComponent>;
  constructor(public dialog: MatDialog) { }

  openDialogOnBoarding() {
  //  this.fileNameDialogRef =
    this.dialog.open(OnboardingQuesComponent);
}
openDialogContract() {
  this.dialog.open(ContractAgreementComponent);
}

  ngOnInit() {
  }

}
