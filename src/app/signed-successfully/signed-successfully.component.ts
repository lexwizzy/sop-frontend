import { Component, OnInit } from '@angular/core';
import { OnboardingQuesComponent } from '../onboarding-ques/onboarding-ques.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-signed-successfully',
  templateUrl: './signed-successfully.component.html',
  styleUrls: ['./signed-successfully.component.css']
})
export class SignedSuccessfullyComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }
  openDialogOnBoarding() {
      this.dialog.open(OnboardingQuesComponent);
  }
}
