import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AgentProfileComponent } from '../agent-profile/agent-profile.component';

@Component({
  selector: 'app-agents',
  templateUrl: './agents.component.html',
  styleUrls: ['./agents.component.css']
})
export class AgentsComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  moreInfo() {
    this.dialog.open( AgentProfileComponent);
}

  ngOnInit() {
  }

}
