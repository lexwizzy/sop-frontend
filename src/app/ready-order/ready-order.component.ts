import { Component, OnInit } from '@angular/core';
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-ready-order',
  templateUrl: './ready-order.component.html',
  styleUrls: ['./ready-order.component.css']
})
export class ReadyOrderComponent implements OnInit {
  faAngleLeft = faAngleLeft;

  constructor() { }

  ngOnInit() {
  }

}
