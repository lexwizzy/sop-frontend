import { Component, OnInit, ElementRef, ViewChild  } from '@angular/core';
import { faAngleLeft, faAngleDown, faPlus, faMinus } from '@fortawesome/free-solid-svg-icons';
import { MDCMenu } from '@material/menu';
import { NgxDrpOptions, PresetItem, Range } from 'ngx-mat-daterange-picker';
@Component({
  selector: 'app-attributes',
  templateUrl: './attributes.component.html',
  styleUrls: ['./attributes.component.css']
})
export class AttributesComponent implements OnInit {
  faAngleLeft = faAngleLeft;
  faAngleDown = faAngleDown;
  faPlus = faPlus;
  faMinus = faMinus;
  matMenuTriggerFor;
  menu;

  range: Range = { fromDate: new Date(), toDate: new Date() };
  options: NgxDrpOptions;
  presets: Array<PresetItem> = [];
  @ViewChild('pickerOne') pickerOne;
  constructor( private elem: ElementRef) { }

ngOnInit() {
  const today = new Date();
  const fromMin = new Date(today.getFullYear(), today.getMonth()-2, 1);
  const fromMax = new Date(today.getFullYear(), today.getMonth()+1, 0);
  const toMin = new Date(today.getFullYear(), today.getMonth()-1, 1);
  const toMax = new Date(today.getFullYear(), today.getMonth()+2, 0);

  this.setupPresets();
  this.options = {
                  presets: this.presets,
                  format: 'mediumDate',
                  range: {fromDate:today, toDate: today},
                  applyLabel: "Submit",
                  calendarOverlayConfig: {
                    shouldCloseOnBackdropClick: false,
                    // hasBackDrop: false
                  }
                  // cancelLabel: "Cancel",
                  // excludeWeekends:true,
                  // fromMinMax: {fromDate:fromMin, toDate:fromMax},
                  // toMinMax: {fromDate:toMin, toDate:toMax}
                };
}
updateRange(range: Range){
  this.range = range;
}
  // helper function to create initial presets
  setupPresets() {
    const backDate = numOfDays => {
      // tslint:disable no-shadowed-variable
      const today = new Date();
      return new Date(today.setDate(today.getDate() - numOfDays));
    };

    const today = new Date();
    const yesterday = backDate(1);
    const minus7 = backDate(7);
    const minus30 = backDate(30);
    const currMonthStart = new Date(today.getFullYear(), today.getMonth(), 1);
    const currMonthEnd = new Date(today.getFullYear(), today.getMonth() + 1, 0);
    const lastMonthStart = new Date(today.getFullYear(), today.getMonth() - 1, 1);
    const lastMonthEnd = new Date(today.getFullYear(), today.getMonth(), 0);

    this.presets = [
      {
        presetLabel: 'Yesterday',
        range: { fromDate: yesterday, toDate: today }
      },
      {
        presetLabel: 'Last 7 Days',
        range: { fromDate: minus7, toDate: today }
      },
      {
        presetLabel: 'Last 30 Days',
        range: { fromDate: minus30, toDate: today }
      },
      {
        presetLabel: 'This Month',
        range: { fromDate: currMonthStart, toDate: currMonthEnd }
      },
      {
        presetLabel: 'Last Month',
        range: { fromDate: lastMonthStart, toDate: lastMonthEnd }
      }
    ];
  }

  reset() {
    const today = new Date();
    const currMonthStart = new Date(today.getFullYear(), today.getMonth(), 1);
    const currMonthEnd = new Date(today.getFullYear(), today.getMonth() + 1, 0);
    this.pickerOne.resetDates({ fromDate: currMonthStart, toDate: currMonthEnd });
  }
  
  
  goBack() {
    window.history.back();
  }
  
@ViewChild('inc') private elementRef: ElementRef;
incrementNumber() {
  var value = parseInt(this.elementRef.nativeElement.value, 10);
  value = isNaN(value) ? 0 : value;
  value++;
  this.elementRef.nativeElement.value = value;
}
decrementNumber() {
  var value = parseInt(this.elementRef.nativeElement.value,10);
  value = isNaN(value) ? 0: value;
  value--;
  this.elementRef.nativeElement.value = value;
}
}
