import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { faPlus, faMinus } from '@fortawesome/free-solid-svg-icons';
import {FormControl} from '@angular/forms';
import { MatMenuTrigger } from '@angular/material';
// import { $ } from 'protractor';
// declare let $
@Component({
  selector: 'app-attribute2',
  templateUrl: './attribute2.component.html',
  styleUrls: ['./attribute2.component.css']
})
export class Attribute2Component implements OnInit {
  
   targetLanguage: string[] = ['English', 'Hindi', 'Polish'];  
  countries: string[] = ['Armenia', 'Germany', 'Poland', 'Swedan', 'Slovakiya']
  socilaMedia: string[] = ['Facebook', 'LinkedIn', 'twitter']
  days: string[] = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']
  faPlus = faPlus;
  faMinus = faMinus;
  checked = false;
  languages = new FormControl()
  //  languageSlectedStr;
  //  langOp: string;

  // public start: Date = new Date ("29/04/2019"); 
  // public end: Date = new Date ("10/12/2019");

  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  constructor( private elem: ElementRef) { }


//   updatelang(event)
//   {
//     var languageSlected=[];
//   $('.mat-checkbox-checked').each(function(ele){
//    languageSlected.push(ele.getAttribute('value'));
//   })
//  this.languageSlectedStr= languageSlected.join(",");
//  console.log(this.languageSlectedStr)
//   }
  someMethod() {
    this.trigger.openMenu();
  }
  ngOnInit() {
  }
  goBack() {
    window.history.back();
  }
  @ViewChild('inc') private elementRef: ElementRef;
  incrementNumber() {
    var value = parseInt(this.elementRef.nativeElement.value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    this.elementRef.nativeElement.value = value;
  }
  decrementNumber() {
    var value = parseInt(this.elementRef.nativeElement.value,10);
    value = isNaN(value) ? 0: value;
    value--;
    this.elementRef.nativeElement.value = value;
  }
  
}
