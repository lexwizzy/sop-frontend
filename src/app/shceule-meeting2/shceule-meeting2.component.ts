import { Component, OnInit } from '@angular/core';
import { faAngleLeft, faAngleDown } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-shceule-meeting2',
  templateUrl: './shceule-meeting2.component.html',
  styleUrls: ['./shceule-meeting2.component.css']
})
export class ShceuleMeeting2Component implements OnInit {
  faAngleLeft = faAngleLeft;
  faAngleDown = faAngleDown;
  constructor() { }

  ngOnInit() {
  }
  goBack() {
    window.history.back();
  }
}
