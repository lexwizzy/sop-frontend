import { Component, OnInit } from '@angular/core';
import { faAngleLeft, faAngleDown, faPlus, faMinus } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-shcedule-meeting',
  templateUrl: './shcedule-meeting.component.html',
  styleUrls: ['./shcedule-meeting.component.css']
})
export class ShceduleMeetingComponent implements OnInit {
  faAngleLeft = faAngleLeft;
  faAngleDown = faAngleDown;
  faPlus = faPlus;
  faMinus = faMinus;
  constructor() { }

  ngOnInit() {
  }
  goBack() {
    window.history.back();
  }
}
