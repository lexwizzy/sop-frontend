import { Component, OnInit } from '@angular/core';
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-review-request',
  templateUrl: './review-request.component.html',
  styleUrls: ['./review-request.component.css']
})
export class ReviewRequestComponent implements OnInit {
  faAngleLeft = faAngleLeft;

  constructor() { }

  ngOnInit() {
  }
  goBack() {
    window.history.back();
  }

}
