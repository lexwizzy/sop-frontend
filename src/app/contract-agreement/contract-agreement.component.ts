import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-contract-agreement',
  templateUrl: './contract-agreement.component.html',
  styleUrls: ['./contract-agreement.component.css']
})
export class ContractAgreementComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<ContractAgreementComponent>,
  ) { }

  ngOnInit() {
  }

}
