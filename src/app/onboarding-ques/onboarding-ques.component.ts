import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-onboarding-ques',
  templateUrl: './onboarding-ques.component.html',
  styleUrls: ['./onboarding-ques.component.css']
})
export class OnboardingQuesComponent implements OnInit {
  constructor(
    private dialogRef: MatDialogRef<OnboardingQuesComponent>,
  ) { }

  ngOnInit() {
  }

}
