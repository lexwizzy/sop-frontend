import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { SolutionsComponent } from './solutions/solutions.component';
import { NormalComponent } from './layouts/normal/normal.component';
import { FullLayoutComponent } from './layouts/full-layout/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout/simple-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { AttributesComponent } from './attributes/attributes.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import { ReviewRequestComponent } from './review-request/review-request.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule, MatListModule, MatSelectModule, MatChipsModule, MatCardModule, MatDialogModule, MatTableModule, MatIconModule} from '@angular/material';
import { Attribute2Component } from './attribute2/attribute2.component';
import { ReadyOrderComponent } from './ready-order/ready-order.component';
import { ThankYouComponent } from './thank-you/thank-you.component';
import { QuoteComponent } from './quote/quote.component';
import { QuoteApprovedComponent } from './quote-approved/quote-approved.component';
import { SignContractComponent } from './sign-contract/sign-contract.component';
import { NgxMatDrpModule } from 'ngx-mat-daterange-picker';
import { OnboardingQuesComponent } from './onboarding-ques/onboarding-ques.component';
import { ContractAgreementComponent } from './contract-agreement/contract-agreement.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { SignedSuccessfullyComponent } from './signed-successfully/signed-successfully.component';
import { AgentsComponent } from './agents/agents.component';
import { ShceduleMeetingComponent } from './shcedule-meeting/shcedule-meeting.component';
import { ShceuleMeeting2Component } from './shceule-meeting2/shceule-meeting2.component';
import { ProjectReadyComponent } from './project-ready/project-ready.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProjectDashboardComponent } from './project-dashboard/project-dashboard.component';
import { SetPasswordComponent } from './set-password/set-password.component';
import { AgentProfileComponent } from './agent-profile/agent-profile.component';
import { HomeComponent } from './home/home.component';
import { SignInComponent } from './auth/sign-in/sign-in.component';
// date range picker
import { DateRangePickerModule, DatePickerModule, TimePickerModule  } from '@syncfusion/ej2-angular-calendars';
import { AgentFormComponent } from './agent-form/agent-form.component';
//  
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  declarations: [
    AppComponent,
    SolutionsComponent,
    NormalComponent,
    FullLayoutComponent,
    SimpleLayoutComponent,
    AuthLayoutComponent,
    AttributesComponent,
    ReviewRequestComponent,
    Attribute2Component,
    ReadyOrderComponent,
    ThankYouComponent,
    QuoteComponent,
    QuoteApprovedComponent,
    SignContractComponent,
    OnboardingQuesComponent,
    ContractAgreementComponent,
    SignedSuccessfullyComponent,
    AgentsComponent,
    ShceduleMeetingComponent,
    ShceuleMeeting2Component,
    ProjectReadyComponent,
    DashboardComponent,
    ProjectDashboardComponent,
    SetPasswordComponent,
    AgentProfileComponent,
    HomeComponent,
    SignInComponent,
    AgentFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    MatCheckboxModule,
    FontAwesomeModule,
    MatButtonModule,
    MatMenuModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatChipsModule,
    MatCardModule,
    NgxMatDrpModule,
    PerfectScrollbarModule,
    MatDialogModule,
    MatTableModule,
    MatIconModule,
    // MatDatepickerModule,
    DatePickerModule,
    DateRangePickerModule,
    TimePickerModule
  ],
  providers: [    
    {
    provide: PERFECT_SCROLLBAR_CONFIG,
    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
  }
],
  entryComponents: [OnboardingQuesComponent,ContractAgreementComponent, SignInComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
