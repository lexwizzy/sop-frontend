import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NormalComponent } from './layouts/normal/normal.component';
import { SolutionsComponent } from './solutions/solutions.component';
import { AttributesComponent } from './attributes/attributes.component';
import { ReviewRequestComponent } from './review-request/review-request.component';
import { Attribute2Component } from './attribute2/attribute2.component';
import { ReadyOrderComponent } from './ready-order/ready-order.component';
import { ThankYouComponent } from './thank-you/thank-you.component';
import { QuoteComponent } from './quote/quote.component';
import { QuoteApprovedComponent } from './quote-approved/quote-approved.component';
import { SignContractComponent } from './sign-contract/sign-contract.component';
import { OnboardingQuesComponent } from './onboarding-ques/onboarding-ques.component';
import { SignedSuccessfullyComponent } from './signed-successfully/signed-successfully.component';
import { AgentsComponent } from './agents/agents.component';
import { ShceduleMeetingComponent } from './shcedule-meeting/shcedule-meeting.component';
import { ShceuleMeeting2Component } from './shceule-meeting2/shceule-meeting2.component';
import { ProjectReadyComponent } from './project-ready/project-ready.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProjectDashboardComponent } from './project-dashboard/project-dashboard.component';
import { SetPasswordComponent } from './set-password/set-password.component';
import { AgentProfileComponent } from './agent-profile/agent-profile.component';
import { HomeComponent } from './home/home.component';
import { AgentFormComponent } from './agent-form/agent-form.component';

const routes: Routes = [
  {
    path:'',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: '',
    component: NormalComponent, children: [
      {
        path: 'solutions',
        component: SolutionsComponent
      },
      {
        path: 'attributes',
        component: AttributesComponent
      },
      {
        path: 'attributes2',
        component: Attribute2Component
      },
      {
        path: 'review-request',
        component: ReviewRequestComponent
      },
      {
        path: 'ready-to-order',
        component:ReadyOrderComponent 
      },
      {
        path: 'thank-you',
        component: ThankYouComponent
      },
      {
        path: 'quote',
        component: QuoteComponent
      },
      {
        path: 'quote-approved',
        component: QuoteApprovedComponent
      },
      {
        path: 'sign-contract',
        component: SignContractComponent
      },
      {
        path: 'onboarding-ques',
        component: OnboardingQuesComponent
      },
      {
        path: 'signed-successfully',
        component: SignedSuccessfullyComponent
      },
      {
        path: 'agents',
        component: AgentsComponent
      },
      {
        path: 'shcedule-meeting',
        component: ShceduleMeetingComponent
      },
      {
        path: 'shcedule-meeting2',
        component: ShceuleMeeting2Component
      },
      {
        path: 'project-ready',
        component: ProjectReadyComponent
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'project-dashboard',
        component: ProjectDashboardComponent
      },
      {
        path: 'set-password',
        component: SetPasswordComponent
      },
      {
        path: 'agent-profile',
        component: AgentProfileComponent
      },
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'agent',
        component: AgentFormComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
